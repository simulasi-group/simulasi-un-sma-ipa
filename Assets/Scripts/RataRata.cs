﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class RataRata : MonoBehaviour {

    float totalNilai;
    public Text mapel1;
    public Text mapel2;
    public Text mapel3;
    public Text mapel4;
    public Text mapel5;
    public Text mapel6;
    public Text rataRata;
    public Text kategori;
    DataUser user;

    void Start ()
    {
        user = GameObject.FindGameObjectWithTag("DataUser").GetComponent<DataUser>();
        float[] nilai =
        {
            (user.jawabanMapel1.Length > 0 ? (user.benarMapel[0] * ((float)100 / user.jawabanMapel1.Length)) : 0),
            (user.jawabanMapel2.Length > 0 ? (user.benarMapel[1] * ((float)100 / user.jawabanMapel2.Length)) : 0),
            (user.jawabanMapel6.Length > 0 ? (user.benarMapel[5] * ((float)100 / user.jawabanMapel6.Length)) : 0),
            (user.jawabanMapel3.Length > 0 ? (user.benarMapel[2] * ((float)100 / user.jawabanMapel3.Length)) : 0),
            (user.jawabanMapel4.Length > 0 ? (user.benarMapel[3] * ((float)100 / user.jawabanMapel4.Length)) : 0),
            (user.jawabanMapel5.Length > 0 ? (user.benarMapel[4] * ((float)100 / user.jawabanMapel5.Length)) : 0)
        };
            
        for(int i = 0;i<user.benarMapel.Length; i++)
            totalNilai += nilai[i];

        mapel1.text = "Nilai UN Bahasa Indonesia\t\t\t\t\t\t\t: " + (user.jawabanMapel1.Length > 0 ? (user.benarMapel[0] * ((float)100 / user.jawabanMapel1.Length)).ToString() : "0");
        mapel2.text = "Nilai UN Bahasa Inggris\t\t\t\t\t\t\t\t\t: " + (user.jawabanMapel2.Length > 0 ? (user.benarMapel[1] * ((float)100 / user.jawabanMapel2.Length)).ToString() : "0");
        mapel3.text = "Nilai UN Matematika\t\t\t\t\t\t\t\t\t\t: " + (user.jawabanMapel6.Length > 0 ? (user.benarMapel[5] * ((float)100 / user.jawabanMapel6.Length)).ToString() : "0");
        mapel4.text = "Nilai UN Kimia\t\t\t\t\t\t\t\t\t\t\t\t\t: " + (user.jawabanMapel3.Length > 0 ? (user.benarMapel[2] * ((float)100 / user.jawabanMapel3.Length)).ToString() : "0");
        mapel5.text = "Nilai UN Fisika\t\t\t\t\t\t\t\t\t\t\t\t\t: " + (user.jawabanMapel4.Length > 0 ? (user.benarMapel[3] * ((float)100 / user.jawabanMapel4.Length)).ToString() : "0");
        mapel6.text = "Nilai UN Biologi\t\t\t\t\t\t\t\t\t\t\t\t: " + (user.jawabanMapel5.Length > 0 ? (user.benarMapel[4] * ((float)100 / user.jawabanMapel5.Length)).ToString() : "0");

        var rata2 = Math.Round((totalNilai / user.benarMapel.Length), 2);

        rataRata.text = "NILAI RATA - RATA UN\t\t\t\t\t\t\t\t\t: " + rata2.ToString();
        if (rata2 > 85)
            kategori.text = "KATEGORI NILAI\t\t\t\t\t\t: \"SANGAT BAIK\"";
        else if (rata2 > 70 && rata2 <= 85)
            kategori.text = "KATEGORI NILAI\t\t\t\t\t\t\t\t\t\t: \"BAIK\"";
        else if (rata2 > 55 && rata2 <= 70)
            kategori.text = "KATEGORI NILAI\t\t\t\t\t\t\t\t\t: \"CUKUP\"";
        else
            kategori.text = "KATEGORI NILAI\t\t\t\t\t\t\t\t: \"KURANG\"";
    }
}
