﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using SimpleJSON;

public class ButtonScript : MonoBehaviour {

	public GameObject soal;

	public string pembahasanUrl = "http://banksoalkompasilmu.com/api/download/";

	public void ChangeScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void StartUN(int indexSoal)
    {
        soal.SetActive(true);
        soal.GetComponentInChildren<Camera>().gameObject.GetComponent<GameData>().StartUN(GetComponent<GetMapel>().id[indexSoal]);
        soal.GetComponentInChildren<Camera>().gameObject.GetComponent<GameData>().mapel = indexSoal;
        soal.GetComponentInChildren<Camera>().gameObject.GetComponent<GameData>().fieldMapel[0].text = GetComponent<GetMapel>().mapel[indexSoal];
        soal.GetComponentInChildren<Camera>().gameObject.GetComponent<GameData>().fieldMapel[1].text = GetComponent<GetMapel>().mapel[indexSoal];
        Destroy(gameObject);
	}

	public void downloadPembahasan(int id) {
		string savePath = Application.persistentDataPath;
		if (File.Exists (savePath + "/Pembahasan_SMA_IPA.pdf") && !isInternetConnection()) {
			Application.OpenURL(savePath+"/Pembahasan_SMA_IPA.pdf");
			return;
		}
		StartCoroutine (getLink (id));
	}

	IEnumerator getLink(int id) {
		WWW www = new WWW(pembahasanUrl + id);
		yield return www;
		if (www.error == null)
		{
			StartCoroutine (Parse (www.text));
		}
		else
		{
			Application.LoadLevel("Home");
			Debug.Log(www.error);
		}
	}

	IEnumerator Parse(string data) {
		var N = JSONNode.Parse(data);
		var path = N ["link"];

		string savePath = Application.persistentDataPath;
		WWW www = new WWW(path);
		yield return www;

		byte[] bytes = www.bytes;
		try{
			File.WriteAllBytes(savePath+"/Pembahasan_SMA_IPA.pdf", bytes);
		}catch(Exception ex){
			Debug.Log (ex.Message);
		}
		Application.OpenURL(path);
	}

	bool isInternetConnection()
	{
		bool isConnectedToInternet = false;
		if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork ||
			Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
		{
			isConnectedToInternet = true;
		}
		return isConnectedToInternet;
	}
}
