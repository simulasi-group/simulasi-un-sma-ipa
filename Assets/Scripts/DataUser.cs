﻿using UnityEngine;
using System.Collections;

public class DataUser : MonoBehaviour {

    private static DataUser instance = null;
    
    public string[] jawabanMapel1, jawabanMapel2, jawabanMapel3, jawabanMapel4, jawabanMapel5, jawabanMapel6;
    public float[] benarMapel;

    public static DataUser Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
}
