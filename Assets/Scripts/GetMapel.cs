﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class GetMapel : MonoBehaviour {
    
    string urlMapel = "http://banksoalkompasilmu.com/api/pelajaran/1";
    public GameObject loading;
    public int[] id;
    public string[] mapel;

    void Start()
    {
        StartCoroutine(Mapel());
    }

    private IEnumerator Mapel()
    {
        string newUrl = urlMapel;
        WWW www = new WWW(newUrl);
        yield return www;
        if (www.error == null)
            ParseMapel(www.text);
        else
        {
            Application.LoadLevel("Home");
            Debug.Log(www.error);
        }

    }

    void ParseMapel(string datas)
    {
        var N = JSONNode.Parse(datas);

        id = new int[N["result"].Count];
        mapel = new string[N["result"].Count];

        for (int i = 0; i < N["result"].Count; i++)
        {
            mapel[i] = N["result"][i]["pelajaran"];
            id[i] = N["result"][i]["id"].AsInt;
        }

        loading.SetActive(false);
    }
}
